package com.stacklabs.blog.testingconfigurationclass;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * Created by kevin on 09/05/2020
 */
@Configuration
public class ExampleConfiguration {

    @Bean
    ExampleService exampleService() {
        return new ExampleService();
    }

}
