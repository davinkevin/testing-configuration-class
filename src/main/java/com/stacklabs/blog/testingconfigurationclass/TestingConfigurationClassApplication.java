package com.stacklabs.blog.testingconfigurationclass;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class TestingConfigurationClassApplication {

	public static void main(String[] args) {
		SpringApplication.run(TestingConfigurationClassApplication.class, args);
	}

}
